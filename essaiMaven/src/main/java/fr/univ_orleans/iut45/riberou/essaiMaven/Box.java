package fr.univ_orleans.iut45.riberou.essaiMaven;

import java.util.ArrayList;

public class Box {

    public class ThingNotFound extends Exception {}
    
    private ArrayList<Thing> contents;
    private String nom;
    private int capacite;
    private boolean open;

    public Box(int capacite) {
        this.contents = new ArrayList<Thing>();
        this.open = false;
        this.capacite=capacite;
    }
    public Box (){
        this.contents = new ArrayList<Thing>();
        this.open = false;
        this.capacite=-1;
    }

    public Box(String nom, int capacite){
        this.nom=nom;
        this.capacite=capacite;
        this.contents = new ArrayList<Thing>();
        this.open = false;
    }

    public Box(String nom, int capacite, boolean open){
        this.nom=nom;
        this.capacite=capacite;
        this.contents = new ArrayList<Thing>();
        this.open = open;
    }

    public void setCapacite(int nombre){
        this.capacite=nombre;
    }
    public int getCapacite(){
        return this.capacite;
    }

    public String getNom(){
        return this.nom;
    }
    public void setNom(String nom){
        this.nom=nom;
    }
    public void ajoute(Thing truc) {
        this.contents.add(truc);
    }

    public boolean supprimer(Thing truc){
        try {
                this.contents.remove(truc);
                return true;
            }

        catch (Exception e) {
            return false;
        }            
    }

    public boolean contient(Thing truc) {
        return this.contents.contains(truc);
    }

    public boolean hasRoomFor(Thing objet){
        if ( this.getCapacite()==-1){
            return true;
        }
        if(this.getCapacite()-objet.getVolume()>=0){
            return true;
        }
        else {return false;}
    }

    public boolean actionAdd(Thing objet){
        if (!this.hasRoomFor(objet) || !this.isOpen()){
            return false;
        }
        else {
            this.ajoute(objet);
            return true;
        }
    }

    public boolean isOpen() {
        return this.open;
    }

    public void open() {
        this.open = true;
    }

    public void close() {
        this.open = false;
    }

    public String actionLook() {
        String chaine="";
        if (!open) {
            return "la boite est fermee";
        } else {
            chaine+="la boite contient : ";
            for (Thing truc : this.contents) {
                chaine+=truc.getNom()+" , ";
            }
        }
        return chaine;
    }

    @Override
    public String toString(){
        return "( "+"nom de la boite : "+this.getNom()+", capacite : "+this.getCapacite()+ ", elle est ouverte : "+this.isOpen()+" et son contenu est : "+this.contenuBoite();
    }
    public boolean bFind(String nom) {
        try{
            if (!this.isOpen()){
                return false;
            }
            for (Thing truc : this.contents) {
                if (truc.getNom() == nom) {
                    return true;
                }
            }
            return false;
        }
        catch(Exception e) {
            return false;
        }
    }

    public String contenuBoite(){
        String res="[ ";
        for (Thing truc: this.contents) {
            res+=truc.getNom()+", volume :"+truc.getVolume()+" | ";
        }
        res+=" ]";
        return res;
    }


}

