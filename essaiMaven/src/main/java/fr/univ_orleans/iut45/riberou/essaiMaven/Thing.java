package fr.univ_orleans.iut45.riberou.essaiMaven;

class Thing{
    private String nom;
    private int volume;
    
    public Thing(String nom, int volume){
        this.nom = nom;
        this.volume=volume;
    }

    public String getNom() {
        return this.nom;
    }

    public int getVolume(){
        return this.volume;
    }

    public void setNom(String nom){
        this.nom=nom;
    }

    public boolean hasNom(String nom) {
        if (this.nom == nom) {return true;}
        return false;
    }
    // public void enregistreThing(String nomFichier){
    //     try {
    //     FileWriter fw = new FileWriter(nomFichier);
    //     fw.write(this.translateEnJson());
    //     }
    //     catch ( IOException e){
    //         e.printStackTrace();
    //     }
    // }

    @Override
    public String toString(){
        return "( "+this.getNom()+" , volume : "+ this.getVolume()+" )";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {return true;}
        if (obj == null) {return false;}
        if (obj instanceof Thing) {
            Thing oui = (Thing) obj;
            return this.getNom() == oui.getNom() && this.getVolume() == oui.getVolume();
        }
        return false;
    }
}
