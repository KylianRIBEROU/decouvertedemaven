package fr.univ_orleans.iut45.riberou.essaiMaven;

import org.junit.*;
import com.google.gson.Gson;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class TestsBoxes{
    @Test
    public void testBoxAdd() {
        Box b = new Box();
        Thing tone = new Thing("truc1", 2);
        Thing ttwo = new Thing("truc2", 4);
        b.ajoute(tone);
        b.ajoute(ttwo);
        assert b.contient(ttwo);
        b.supprimer(ttwo);
        assertFalse("le boite ne contient plus ttwo", b.contient(ttwo));

        Box boiteDetest = new Box (14);
        Thing marteau = new Thing ( "marteau", 8);
        Thing lanceRoquettes = new Thing ("Lance Roquettes", 17);
        assertFalse("la boite a pas de place pour le lance roro", boiteDetest.hasRoomFor(lanceRoquettes));

        boiteDetest.close();
        assertFalse("la boite est fermée", boiteDetest.isOpen());
        System.out.println(boiteDetest.actionLook());
        boiteDetest.open();
        boiteDetest.ajoute(marteau);
        assertTrue(" test que marteau est bien dans boite ", boiteDetest.bFind("marteau"));
    }
        public static void main(String[] args) {
            
        
        try{
            FileReader file = new FileReader("boxes.json");
            Gson gson = new Gson();
            Box laBoiteJSON = gson.fromJson(file, Box.class);
            System.out.println(laBoiteJSON.toString());

            FileReader fileThings = new FileReader("Things.json");
            Thing laPremiereThing = gson.fromJson(fileThings, Thing.class);
            System.out.println(laPremiereThing.toString());

            Thing newTest = new Thing("Madeleines", 4);
            FileWriter fw = new FileWriter("TestJson.json");
            
        }

        catch (IOException e) { 
             e.printStackTrace();
             }
    
} }
